# Wordpress

This is the documentation needed to set up and run Wordpress.

## Prerequisites

For this, you will need a kubernetes cluster consisting of one control panel, two nodes and one machine for Ansible to be installed. We have made a cluster but should you choose to create another, all three machines must:
- Be an Ubuntu Server 20.04 LTS (HVM)
- Be instance type t3a.large
- Have 30Gib of storage
- Be appropriately tagged (for easy identification)
- Be using AllowJenkinsMachines and Cohort9-homeips security groups (your IP should be added to these security groups if not already) or create you own which have the same rules
- Have a key pair so that you are able to ssh into the machines

If you are using the existing cluster, you must add you IPv4 address to the security groups.

Once the three instances have been launched, you need to ssh into the machine ready for Ansible and create a new key pair:

```bash
$ ssh-keygen 
> Add a path/name
> No password
```
Copy the key which has a name eding in '.pub' and, in new tabs in the terminal, ssh into your three other machines. Change directory to `.ssh` and append the public key to the authorized_keys file. This will allow you to ssh into the three machines from the ansible machine using the new key and private IP addresses of the machines.

Next, from the ansible machine, clone this repo:
```bash
$ git clone https://github.com/kubernetes-sigs/kubespray.git
```

Then:
```bash
$ cd kubespray
$ sudo apt update
$ sudo apt install python3-pip -y
```

Run the requirements.txt:
```bash
sudo pip3 install -r requirements.txt
```

Note: this repo should install ansible but may need these dependencies:
```bash
# python3
# git
```

Kubespray has a simple inventory file that we can copy and edit:
```bash
# Copy ``inventory/sample`` as ``inventory/mycluster``
cp -rfp inventory/sample inventory/mycluster
```


```bash
$ cd inventory/mycluster 
$ nano inventory.ini
```
In the first block of code in this file, nodes 1, 2 and 3 need to be uncommented and the IP addresses need to be replaced by the private IPs of the control panel, node a and node b; respectively. In addition, `ansible_ssh_private_key_file=/home/ubuntu/.ssh/asses_4_key ansible_user=ubuntu` needs to be appended to these lines. In the second block of code, under the title 'control-panel', uncomment the first node since there is only one control panel. Under 'etcd', uncomment all three nodes and under 'kube_node' uncomment nodes 2 and 3. Save and exit.

Change directory so that you are in the Kubespray folder and run:
```bash
# Update Ansible inventory file with inventory builder
declare -a IPS=(<private IP> <private IP> <privateIP>) #pvt IPs of instances

CONFIG_FILE=inventory/mycluster/hosts.yaml python3 contrib/inventory_builder/inventory.py ${IPS[@]}

sudo apt install ansible -y

ansible-playbook -i ~/kubespray/inventory/mycluster/inventory.ini --become-user=root ~/kubespray/cluster.yml -b  --private-key=~/.ssh/k8_key.pem
# Review and change parameters under ``inventory/mycluster/group_vars``
cat inventory/mycluster/group_vars/all/all.yml
cat inventory/mycluster/group_vars/k8s_cluster/k8s-cluster.yml
```

Now from the control panel machine, run the following command in order to start the kubectl service:
```bash
$sudo cp /etc/kubernetes/admin.conf $HOME/ && sudo chown $(id -u):$(id -g) $HOME/admin.conf && export KUBECONFIG=$HOME/admin.conf
```

In addition to this, the files in this repo need to be copied to the control panel machine. The main files are the storage, persistent volume (pv), the deployment files and the persistent volume claim (pvc, within the deployment files); all of which are created with yaml configuration. Volumes are needed in order for  data to be persistent. The pvc claims a volume with a certain capacity which is defined in the pvc file. Whichever pv satisfies the claim will be used for the application. 

Persistent volumes are resources just like CPU or RAM and are available to the whole cluster, across all namespaces. The pods depend on them and so they need to be available before the pods are created. To ensure this, first apply the storage and pv files:
```bash
kubectl apply -f storage.yaml -n <your_namespace>
kubectl apply -f pv.yaml -n <your_namespace>
```

Next apply the deployments, these have the claim within the file as well as creating the pods:
```bash
kubectl apply -f mysql-deployment.yaml -n <your_namespace>
kubectl apply -f wordpress-deployment.yaml -n <your_namespace>
```

To check that everything is up and running, run the following commands:
```bash
kubectl get pv -n <your_namespace>
kubectl get pvc -n <your_namespace>
kubectl get pods -n <your_namespace>
```

The pv and pvc should have a status of 'bound' and any pods should be running.

Finally, if you run `kubectl get services -n <your_namespace>```, you should be able to see the loadbalancer. If you take the public IP address of either node a or b and add the port provided by the loadbalancer, you should be able to see and use Wordpress.

If this doesn't work, we created a loadbalancer on AWS. Therefore, Wordpress can be accessed via:
http://fernandos.wordpress.academy.labs.automationlogic.com/wp-admin/install.php

Sometimes a 502 error might appear but this can be resolved by refreshing the page.

A nodeport file has also been created and, when applied, can be used as another method of accessing Wordpress. You will need the public IP address of either node a or node b and the port specified in the file. E.g.: 54.76.90.174:30093

